# eth-data

A repository for all the data I gather during my research

### Contents

  * `precompiles` : research data on WASM precompiles
    * `expmod_usage.csv` : Rinkeby `(block number, input, output)` data for the expmod precompile up to block `5530817`;
